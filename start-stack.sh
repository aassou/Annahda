#!/usr/bin/env bash


echo
echo
echo "####################################"
echo "### Building the Web Application ###"
echo "####################################"
docker build -t annahda_app -f docker/DockerfileApp .


echo
echo
echo "####################################"
echo "### Building the DB  Application ###"
echo "####################################"
docker build -t annahda_db -f docker/DockerfileDB .

echo
echo
echo "####################################"
echo "###  Building the PHP  MyAdimin  ###"
echo "####################################"

docker-compose up -d